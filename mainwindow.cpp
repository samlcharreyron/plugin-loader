#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::loadPlugins(QString pluginDir)
{
    QDir pluginsDir(pluginDir);

    pluginsDir.setNameFilters(QStringList()<<"*.so");
    foreach (QString fileName, pluginsDir.entryList(QDir::Files))
    {
        // Create plugin loader
        QPluginLoader* pluginLoader =
                new QPluginLoader(pluginsDir.absoluteFilePath(fileName));
        // Store loader to array
        plugins.append(pluginLoader);
        // Load plugin
        bool ret = pluginLoader->load();
        if (!ret)
        {
            qDebug() << pluginLoader->errorString();
            QMessageBox::information(this, "Plugin Loader",
                                     QString("Could not load plugin %1").arg(fileName));
        }
    }

    // Did we found any plugins?
    if (plugins.isEmpty())
        return false;
    else
        return true;
}

void MainWindow::createPlugins()
{
    // Show data of all media plugins in different tabs
    for (int i=0 ; i<plugins.count() ; i++)
    {
        QPluginLoader* pluginLoader = plugins[i];
        // Create plugin instance
        QObject *plugin = pluginLoader->instance();
        if (plugin)
        {
            // Plugin instance created

            // Cast plugin to ExamplePluginInterface,
            // that is common for all plugins
            DInterfacePlugin* pluginIF
                    = qobject_cast<DInterfacePlugin*>(plugin);
            ui->tabWidget->addTab(pluginIF,pluginIF->objectName());
            // Signal / slot, if needed
            //QObject::connect(this, SIGNAL(send(QString *)),
            //        pluginIF, SLOT(someSlot(QString *)));
        }
        else
        {
            // Could not create plugin instance, delete pluginloader
            delete pluginLoader;
            plugins.removeAt(i);
            i--;
        }
    }
}

void MainWindow::unloadPlugins()
{

    // Unload plugins and clear plugin array
    foreach (QPluginLoader* pluginLoader, plugins)
    {
        pluginLoader->unload();
        delete pluginLoader;
    }
    plugins.clear();

}



void MainWindow::on_loadButton_released()
{
    QString folderName = QFileDialog::getExistingDirectory();
    if (loadPlugins(folderName)) {
        createPlugins();
    }
}
