#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPluginLoader>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <Daedalus/dinterfaceplugin.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_loadButton_released();

private:
    Ui::MainWindow *ui;
    bool loadPlugins(QString pluginDir);
    void createPlugins();
    void unloadPlugins();

    QList<QPluginLoader*>   plugins;

};

#endif // MAINWINDOW_H
