#-------------------------------------------------
#
# Project created by QtCreator 2014-03-24T19:39:12
#
#-------------------------------------------------

QT       += core gui

include(/home/samuelch/src/examples/examples.pri)

TARGET = pluginLoader
TEMPLATE = app

CONFIG += thread

QT_DEBUG_PLUGINS = 1

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

OTHER_FILES +=
